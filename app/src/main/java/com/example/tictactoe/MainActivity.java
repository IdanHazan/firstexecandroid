package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    public final int EMPTY_CELL_VALUE = 2;
    public final int O_CELL_VALUE = 0;
    public final int X_CELL_VALUE = 1;
    public final int NUM_CELLS = 9;
    public int[] boardStatus = new int[NUM_CELLS];
    public int playerTurn = O_CELL_VALUE;
    public final int[] WINNING_MARKS_IMAGES = {R.drawable.mark8, R.drawable.mark7, R.drawable.mark6, R.drawable.mark3, R.drawable.mark4, R.drawable.mark5,
            R.drawable.mark1, R.drawable.mark2};
    final int[][] WINNING_POSITIONS = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    public int turnCounter; // Turn number


    public void reflectActionOnUI(ImageView clickedImage, int clickedImageSource, int turnImageSource) {
        clickedImage.setClickable(false);
        clickedImage.setImageResource(clickedImageSource);
        ImageView playerTurnImg = findViewById(R.id.main_turn_img);
        playerTurnImg.setImageResource(turnImageSource);
    }

    public void declareWinner(int winner, int[] winningPositions) {
        ImageView winnerImg = findViewById(R.id.main_winner_img);
        ImageView winMarkImg = findViewById(R.id.main_win_mark_img);
        Button playAgainBtn =(Button) findViewById(R.id.play_again_btn);
        winnerImg.setImageResource(winner == EMPTY_CELL_VALUE ? R.drawable.nowin : (winner == O_CELL_VALUE ? R.drawable.owin : R.drawable.xwin));

        // Game has a winner
        if (winner != EMPTY_CELL_VALUE) {
            winMarkImg.setImageResource(WINNING_MARKS_IMAGES[Arrays.asList(WINNING_POSITIONS).indexOf(winningPositions)]);
        }

        playAgainBtn.setVisibility(View.VISIBLE);
        playAgainBtn.setClickable(true);
        winMarkImg.setClickable(true);
    }

    public void playerAction(ImageView clickedImage) {
        int clickedCellImage = playerTurn == O_CELL_VALUE ? R.drawable.o : R.drawable.x;
        int squareNumber = Integer.parseInt(clickedImage.getTag().toString());
        turnCounter++;
        boardStatus[squareNumber] = playerTurn; // Set clicked square to the player that played this turn
        int[] winningPosition = checkWinningStatus(boardStatus);
        // A player won or game has finished with a draw
        if (winningPosition != null || turnCounter == NUM_CELLS) {
            declareWinner(winningPosition == null ? EMPTY_CELL_VALUE : playerTurn, winningPosition);
        } else {
            playerTurn = playerTurn == O_CELL_VALUE ? X_CELL_VALUE : O_CELL_VALUE; // Switch turn to next player
        }
        reflectActionOnUI(clickedImage,
                clickedCellImage,
                playerTurn == O_CELL_VALUE ? R.drawable.oplay : R.drawable.xplay);

    }

    public void startGame() {
        Button playAgainBtn =(Button) findViewById(R.id.play_again_btn);
        playAgainBtn.setVisibility(View.GONE);
        Arrays.fill(boardStatus, EMPTY_CELL_VALUE);
        ImageView winMarkImg = findViewById(R.id.main_win_mark_img);
        ImageView winnerImg = findViewById(R.id.main_winner_img);
        winnerImg.setImageResource(R.drawable.empty);
        winMarkImg.setImageResource(R.drawable.empty);
        winMarkImg.setOnClickListener(e -> {
            startGame();
        });
        playAgainBtn.setOnClickListener(e -> {
            startGame();
        });
        winMarkImg.setClickable(false);
        turnCounter = 0;
        // Setting click event listeners to all Buttons
        for (int i = 1; i <= 9; i++) {

            ImageView img = findViewById(getResources().getIdentifier("main_cell" + i + "_img", "id", getPackageName()));
            img.setImageResource(R.drawable.empty);
            img.setClickable(true);
            img.setOnClickListener(e -> {
//                e.setClickable(false);
                playerAction((ImageView) e);
            });
        }

    }


    public int[] checkWinningStatus(int[] boardStatus) {
        for (int i = 0; i < WINNING_POSITIONS.length; i++) {
            if (boardStatus[WINNING_POSITIONS[i][0]] == boardStatus[WINNING_POSITIONS[i][1]] &&
                    boardStatus[WINNING_POSITIONS[i][0]] == boardStatus[WINNING_POSITIONS[i][2]] &&
                    boardStatus[WINNING_POSITIONS[i][0]] != EMPTY_CELL_VALUE) {
                return WINNING_POSITIONS[i];
            }
        }
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize all relevant data & settings
        startGame();
    }
}